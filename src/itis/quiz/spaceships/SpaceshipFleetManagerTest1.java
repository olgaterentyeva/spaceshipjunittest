package itis.quiz.spaceships;

import java.util.ArrayList;

public class SpaceshipFleetManagerTest1 {
    SpaceshipFleetManager center;
    ArrayList<Spaceship> testList = new ArrayList<>();

    public static void main(String[] args) {
        SpaceshipFleetManagerTest1 test = new SpaceshipFleetManagerTest1(new CommandCenter());
        System.out.println("You have earned " + counter(test)+ " point(s)!");
    }

    private static int counter(SpaceshipFleetManagerTest1 test) {
        int all_count;
        all_count = testGetMostPowerfulShip(test) + testGetShipByName(test) + testGetAllShipsWithEnoughCargoSpace(test) + testGetAllCivilianShips(test);
        return all_count;
    }


    private static int testGetMostPowerfulShip(SpaceshipFleetManagerTest1 test) {
        int count = 0;
        System.out.println("Test for get most powerful ship (№1)");
        boolean res1 = test.getMostPowerfulShip_shipWithMostPowerExists_returnShip();
        System.out.println("test_1 >> " + res1);
        boolean res2 = test.getMostPowerfulShip_shipWithMostPowerExists_returnFirstShip();
        System.out.println("test_2 >> " + res2);
        boolean res3 = test.getMostPowerfulShip_shipWithMostPowerExists_returnNull();
        System.out.println("test_3 >> " + res3);
        if (res1 && res2 && res3) {
            count++;
        }
        return count;
    }

    private static int testGetShipByName(SpaceshipFleetManagerTest1 test) {
        int count = 0;
        System.out.println("Test for get ship by name (№2)");
        boolean res1 = test.getShipByName_shipExists_returnTargetShip();
        System.out.println("test_1 >> " + res1);
        boolean res2 = test.getShipByName_shipNotExists_returnNull();
        System.out.println("test_2 >> " + res2);
        if (res1 && res2) {
            count++;
        }
        return count;
    }

    private static int testGetAllShipsWithEnoughCargoSpace(SpaceshipFleetManagerTest1 test) {
        int count = 0;
        System.out.println("Test for get all ships without cargo space (№3)");
        boolean res1 = test.getAllShipsWithEnoughCargoSpace_shipsWithEnoughCargoSizeExists_returnListOfShips();
        System.out.println("test_1 >> " + res1);
        boolean res2 = test.getAllShipsWithEnoughCargoSpace_shipsWithEnoughCargoSizeNotExists_returnNullList();
        System.out.println("test_2 >> " + res2);
        if (res1 && res2) {
            count++;
        }
        return count;
    }

    private static int testGetAllCivilianShips(SpaceshipFleetManagerTest1 test) {
        int count = 0;
        System.out.println("Test for get all civilian ships (№4)");
        boolean res1 = test.getAllCivilianShips_shipsWithoutPowerExists_returnCivilianShips();
        System.out.println("test_1 >> " + res1);
        boolean res2 = test.getAllCivilianShips_shipsWithoutPowerNotExists_returnNull();
        System.out.println("test_2 >> " + res2);
        if (res1 && res2) {
            count++;
        }
        return count;
    }

    private boolean getMostPowerfulShip_shipWithMostPowerExists_returnShip() { // 1
        testList.add(new Spaceship("Foo", 100, 0, 10));
        testList.add(new Spaceship("Bar", 90, 0, 10));
        testList.add(new Spaceship("Mac", 60, 0, 10));

        Spaceship result = center.getMostPowerfulShip(testList);
        if (result != null && result.getFirePower() == 100) {
            testList.clear();
            return true;
        }
        testList.clear();
        return false;
    }

    private boolean getMostPowerfulShip_shipWithMostPowerExists_returnFirstShip() { // 2
        testList.add(new Spaceship("Foo", 100, 0, 10));
        testList.add(new Spaceship("Bar", 100, 0, 10));
        testList.add(new Spaceship("Mac", 60, 0, 10));

        Spaceship result = center.getMostPowerfulShip(testList);
        if (result != null && result.getFirePower() == 100 && result.getName().equals("Foo")) {
            testList.clear();
            return true;
        }
        testList.clear();
        return false;
    }

    private boolean getMostPowerfulShip_shipWithMostPowerExists_returnNull() { // 3
        testList.add(new Spaceship("Foo", 0, 0, 10));
        testList.add(new Spaceship("Bar", 0, 0, 10));
        testList.add(new Spaceship("Mac", 0, 0, 10));

        Spaceship result = center.getMostPowerfulShip(testList);
        if (result == null) {
            testList.clear();
            return true;
        }
        testList.clear();
        return false;
    }

    private boolean getShipByName_shipExists_returnTargetShip() { // 4
        testList.add(new Spaceship("Foo", 100, 0, 10));
        testList.add(new Spaceship("Bar", 100, 0, 10));
        testList.add(new Spaceship("Mac", 100, 0, 10));

        Spaceship result = center.getShipByName(testList, "Bar");
        if (result != null && result.getName().equals("Bar")) {
            testList.clear();
            return true;
        }
        testList.clear();
        return false;
    }

    private boolean getShipByName_shipNotExists_returnNull() { // 5
        testList.add(new Spaceship("Foo", 100, 0, 10));
        testList.add(new Spaceship("Bar", 100, 0, 10));
        testList.add(new Spaceship("Mac", 100, 0, 10));

        Spaceship result = center.getShipByName(testList, "Noun");
        if (result == null) {
            testList.clear();
            return true;
        }
        testList.clear();
        return false;
    }

    private boolean getAllShipsWithEnoughCargoSpace_shipsWithEnoughCargoSizeExists_returnListOfShips() { // 6
        testList.add(new Spaceship("Foo", 100, 10, 10));
        testList.add(new Spaceship("Bar", 100, 35, 10));
        testList.add(new Spaceship("Mac", 100, 0, 10));

        int testCargoSize = 32;
        ArrayList<Spaceship> result = center.getAllShipsWithEnoughCargoSpace(testList, testCargoSize);
        for (Spaceship spaceship : result) {
            if (spaceship.getCargoSpace() >= testCargoSize) {
                testList.clear();
                return true;
            }
        }
        testList.clear();
        return false;
    }

    private boolean getAllShipsWithEnoughCargoSpace_shipsWithEnoughCargoSizeNotExists_returnNullList() { // 7
        testList.add(new Spaceship("Foo", 100, 3, 10));
        testList.add(new Spaceship("Bar", 100, 0, 10));
        testList.add(new Spaceship("Mac", 100, 2, 10));

        ArrayList<Spaceship> result = center.getAllShipsWithEnoughCargoSpace(testList, 10);

        if (result == null) {
            testList.clear();
            return true;
        }
        testList.clear();
        return false;
    }


    public boolean getAllCivilianShips_shipsWithoutPowerExists_returnCivilianShips() { // 8
        testList.add(new Spaceship("Foo", 60, 0, 10));
        testList.add(new Spaceship("Bar", 0, 0, 10));
        testList.add(new Spaceship("Mac", 0, 0, 10));

        ArrayList<Spaceship> result = center.getAllCivilianShips(testList);
        for (Spaceship spaceship : result) { // этот тест написан по - другому
            if (spaceship.getFirePower() == 0) {
                testList.clear();
                return true;
            }
        }
        testList.clear();
        return false;
    }

    public boolean getAllCivilianShips_shipsWithoutPowerNotExists_returnNull() { // 9
        testList.add(new Spaceship("Foo", 60, 0, 10));
        testList.add(new Spaceship("Bar", 40, 0, 10));
        testList.add(new Spaceship("Mac", 50, 0, 10));

        ArrayList<Spaceship> result = center.getAllCivilianShips(testList);
        if (result == null) {
            testList.clear();
            return true;
        }
        testList.clear();
        return false;
    }

    public SpaceshipFleetManagerTest1(SpaceshipFleetManager center) {
        this.center = center;
    }
}
