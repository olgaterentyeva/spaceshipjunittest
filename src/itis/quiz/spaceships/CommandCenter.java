package itis.quiz.spaceships;


import java.util.ArrayList;
import java.util.Comparator;

// Вы проектируете интеллектуальную систему управления ангаром командного центра.
// Реализуйте интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используйте СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Spaceship).
public class CommandCenter implements SpaceshipFleetManager {

    public CommandCenter() {}


    @Override
    public Spaceship getMostPowerfulShip(ArrayList<Spaceship> ships) {
        Spaceship spaceship = new Spaceship("null", 0, 0, 0);
        for (Spaceship ship : ships) {
            if (ship.getFirePower() > spaceship.getFirePower()) {
                spaceship = ship;
            }
        }
        if (spaceship.getFirePower() == 0)
            return null;
        else return spaceship;
    }


    @Override
    public Spaceship getShipByName(ArrayList<Spaceship> ships, String name) {
        for (Spaceship ship : ships) {
            if (ship.getName().equals(name)) {
                return ship;
            }
        }
        return null;
    }

    @Override
    public ArrayList<Spaceship> getAllShipsWithEnoughCargoSpace(ArrayList<Spaceship> ships, Integer cargoSize) {
        ArrayList<Spaceship> ar = new ArrayList<>();
        for (Spaceship ship : ships) {
            if (ship.getCargoSpace() >= cargoSize) {
                ar.add(ship);
            }
        }
        if (ar.isEmpty()) {
            return null;
        }
        return ar;
    }

    @Override
    public ArrayList<Spaceship> getAllCivilianShips(ArrayList<Spaceship> ships) {
        ArrayList<Spaceship> ar = new ArrayList<>();
        for (Spaceship ship : ships) {
            if (ship.getFirePower() == 0) {
                ar.add(ship);
            }
        } if (ar.isEmpty()) {
            return null;
        }
        return ar;
    }
}

