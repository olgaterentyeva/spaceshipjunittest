package itis.quiz.spaceships;
import org.junit.jupiter.api.*;
import java.util.ArrayList;

public class SpaceshipFleetManagerTest2 {

    static SpaceshipFleetManager spaceshipFleetManager = new CommandCenter();
    ArrayList<Spaceship> testList = new ArrayList<>();

    @AfterAll
    static void afterAll() {
        System.out.println("The end!");
    }

    @AfterEach
    void afterEach() {
        testList.clear();
    }

    @DisplayName("Возвращает корабль с самой большой огневой мощью")
    @Test // 1 - 1
    void mostPowerfulShip_shipExists_returnShip() {
        testList.add(new Spaceship("Bar", 90, 0, 10));
        testList.add(new Spaceship("Mac", 60, 0, 10));
        Spaceship foo = new Spaceship("Foo", 100, 0, 10);
        testList.add(foo);

        Spaceship result = spaceshipFleetManager.getMostPowerfulShip(testList);
        Assertions.assertEquals(foo, result);
    }

    @DisplayName("Возвращает первый корабль с самой большой огневой мощью")
    @Test // 1 - 2
    void mostPowerfulShip_shipExists_returnFirstShip() {
        testList.add(new Spaceship("Mac", 60, 0, 10));
        Spaceship bar = new Spaceship("Bar", 100, 10, 0);
        Spaceship foo = new Spaceship("Foo", 100, 0, 10);
        testList.add(foo);
        testList.add(bar);

        Spaceship result = spaceshipFleetManager.getMostPowerfulShip(testList);
        Assertions.assertEquals(foo, result);
    }

    @DisplayName("Возвращает null, если корабль не найден")
    @Test // 1 - 3
    void mostPowerfulShip_shipNotExists_returnNull() {
        testList.add(new Spaceship("Foo", 0, 0, 10));
        testList.add(new Spaceship("Bar", 0, 0, 10));
        testList.add(new Spaceship("Mac", 0, 0, 10));

        Assertions.assertNull(spaceshipFleetManager.getMostPowerfulShip(testList));
    }

    @DisplayName("Возвращает корабль с заданным именем")
    @Test // 2 - 1
    void shipByName_shipExists_returnShip() {
        testList.add(new Spaceship("Foo", 100, 0, 10));
        testList.add(new Spaceship("Mac", 100, 0, 10));
        Spaceship bar = new Spaceship("Bar", 100, 0, 10);
        testList.add(bar);

        Spaceship result = spaceshipFleetManager.getShipByName(testList, "Bar");
        Assertions.assertEquals(bar, result);
    }

    @DisplayName("Возвращает null, если нет кораблей с таким именем")
    @Test // 2 - 2
    void shipByName_shipNotExists_returnNull() {
        testList.add(new Spaceship("Foo", 100, 0, 10));
        testList.add(new Spaceship("Bar", 100, 0, 10));
        testList.add(new Spaceship("Mac", 100, 0, 10));

        Assertions.assertNull(spaceshipFleetManager.getShipByName(testList, "Noun"));
    }

    @DisplayName("Возвращает корабли с нужной грузоподъемностью")
    @Test // 3 - 1
    void byEnoughCargoSpace_shipExists_returnListOfShips() {
        testList.add(new Spaceship("Foo", 100, 10, 10));
        testList.add(new Spaceship("Bar", 100, 35, 10));
        testList.add(new Spaceship("Mac", 100, 0, 10));

        ArrayList<Spaceship> testing = new ArrayList<>();
        testing.add(testList.get(1));

        Assertions.assertEquals(testing, spaceshipFleetManager.getAllShipsWithEnoughCargoSpace(testList, 32));
    }

    @DisplayName("Возвращает null, если кораблей с большим грузовым трюмом нет")
    @Test // 3 - 2
    void byEnoughCargoSpace_shipNotExists_returnEmptyList() {
        testList.add(new Spaceship("Foo", 100, 3, 10));
        testList.add(new Spaceship("Bar", 100, 0, 10));
        testList.add(new Spaceship("Mac", 100, 2, 10));

        Assertions.assertNull(spaceshipFleetManager.getAllShipsWithEnoughCargoSpace(testList, 10));
    }

    @DisplayName("Возвращает мирные корабли")
    @Test // 4 - 1
    void civilianShips_shipExists_returnCivilianShips() {
        testList.add(new Spaceship("Foo", 60, 0, 10));
        testList.add(new Spaceship("Bar", 0, 0, 10));
        testList.add(new Spaceship("Mac", 0, 0, 10));

        ArrayList<Spaceship> testing = new ArrayList<>();
        testing.add(testList.get(1));
        testing.add(testList.get(2));

        Assertions.assertEquals(testing, spaceshipFleetManager.getAllCivilianShips(testing));
    }

    @DisplayName("Возвращает null, если мирных кораблей нет")
    @Test // 4 - 2
    void civilianShips_shipNotExists_returnEmptyList() {
        testList.add(new Spaceship("Foo", 60, 0, 10));
        testList.add(new Spaceship("Bar", 40, 0, 10));
        testList.add(new Spaceship("Mac", 50, 0, 10));

        Assertions.assertNull(spaceshipFleetManager.getAllCivilianShips(testList));
    }
}
